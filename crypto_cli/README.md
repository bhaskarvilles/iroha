# Iroha Crypto CLI

## Description

The tool to generate keys for Iroha peers and clients.
Pay attention: the public key is represented in [multihash format](https://github.com/multiformats/multihash).

## Build
```bash
cargo build
```

## Usage
```bash
./iroha_crypto_cli --help
```